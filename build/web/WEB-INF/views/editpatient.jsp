<%-- 
    Document   : editpatient
    Created on : Apr 2, 2019, 4:42:54 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>editpatient</title>
    </head>
    <body>
       
        <form method="POST">
            
          <h2 style="color: blue" align='center'> Please edit patient's information </h2>
          <table style="background: white" align="center"> 
            
            <tr><td>Registration Number</td><td> <input type='text' class='form-control' name='registrationnumber' value="${patient.registrationnumber}"> 
            <div id="reg_error" class="val_error"</div>
            </td></tr>
            <tr><td>Health Care Unit</td><td> <input type='text' name='healthcareunit' value="${patient.healthcareunit}">
            <div id="unit_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Patient Name </td><td> <input type='text' name='patientname' value="${patient.patientname}">
             <div id="name_error" class="val_error"</div>       
            </td></tr>
            <tr><td>Gender</td><td><select name='gender' value="${patient.gender}">
                        <option value="">--Select Gender--</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </select>
            <div id="gender_error" class="val_error"</div>   
            </td></tr>
            <tr><td>Date of Birth</td><td> <input type='date' name='dateofbirth' value="${patient.dateofbirth}">
            <div id="dob_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Home Address </td><td> <input type='text' name='homeaddress' value="${patient.homeaddress}">
            <div id="address_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Occupation </td><td> <input type='text' name='occupation' value="${patient.occupation}">
            <div id="occupation_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Religion </td><td> <input type='text' name='religion' value="${patient.religion}">
            <div id="religion_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Tribe </td><td> <input type='text' name='tribe' value="${patient.tribe}">
            <div id="tribe_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Next Of Kin </td><td> <input type='text' name='nextofkin' value="${patient.nextofkin}">
            <div id="kin_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Contact of NOK </td><td> <input type='number' name='contactpersonphone' value="${patient.contactpersonphone}">
            <div id="contact_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Blood Group</td><td> <input type='text' name='bloodgroup' value="${patient.bloodgroup}">
            <div id="blood_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Known Allergies</td><td> <input type='text' name='allergies' value="${patient.allergies}">
            <div id="allergy_error" class="val_error"</div>
            </td></tr>
            <tr><td></td><td> <input type='submit' value = "update" ></td></tr>
            <tr><td></td><td> <input type='reset' value = "Reset" ></td></tr>
            <c:choose>
                <c:when test="${empty patient.patientid}">
                    <input type="hidden" name="patientid" value="0"> 
                </c:when>
                <c:otherwise>
                 <input type="hidden" name="patientid" value="${patient.patientid}">
                </c:otherwise>
            </c:choose>
            </form>
        </table>        
    </body>
</html>
