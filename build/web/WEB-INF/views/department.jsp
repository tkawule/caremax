<%-- 
    Document   : department
    Created on : Aug 6, 2018, 12:28:41 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Department</title>
    </head>
    <body>
        <h1>Department form</h1>
        <form method="POST" action="/CareMax/department">
            <table>
                <tr><td>Department Name</td><td> <input type='text' name='dep_name'></td></tr>
                <tr><td>Department Head</td><td> <input type='text' name='dep_head'></td></tr>
                <tr><td></td><td> <input type='submit' value = "Submit" ></td></tr>
                <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr>
                
            </table>
        </form>
    </body>
</html>
