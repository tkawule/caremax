<%-- 
    Document   : treatmentlist
    Created on : Jan 3, 2019, 12:43:19 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <style>
BODY, TABLE, TH, TD, TR, FORM {font: 12px Tahoma,Arial,Helvetica,sans-serif; vertical-align: top}
		FORM {margin: 0px; padding: 0px}
		TEXTAREA, INPUT, SELECT, LABEL, BUTTON {font: 11px Tahoma,Arial,Helvetica,sans-serif }
		TD.label { text-align: right; vertical-align: middle  }
		TABLE {border-collapse: collapse     }
		TH {font: bold; background: #FFF3CB; border: 1px solid #D6D6D6; padding: 5px; text-align: center}
		.bordered {border: 1px solid #D6D6D6 }
		.evenrow  {background-color: #EFEFEF }
</style>

<script>
   function FilterRow(data) {
    console.log("searching for "+data); 
    var filter = data;
    var table = document.getElementById("datatable");
  for (i = 1; i<table.rows.length; i++)
    {
        var rowdata = table.rows[i].innerHTML;
        console.log(rowdata.toUpperCase());
         if(rowdata.toUpperCase().search(data.toUpperCase())>-1) {table.rows[i].style.display="";} 
        else {table.rows[i].style.display="none";} 
    }
    
}
</script> 
        <a style="align-content: center" href="<c:url value='/treatment'/> ">Add Treatment</a> 
       
        <hr> 
        <table border="1" width="95%" align="center">
            <tr>
                <td><input style="min-width:95%" type="text" onkeydown="FilterRow(this.value)" placeholder="Search for a Patient"/></td>
            </tr>
        </table><br/>
        
        <table  id="datatable" style="width: 95%" align="center" border="1">
            <thead>
            <tr bgcolor="#cccccc">
            <tr>
                
                <th>Diagnosis</th>
                <th>Treatment</th>
                <th>Dosage</th>
                <th>Action</th>
            </tr>
            
            <c:forEach items="${treatments}" var="treatment" varStatus="theCount">
                
                <tr>
                    
                    <td>${treatment.diagnosis}</td>
                    <td>${treatment.treatment}</td>
                    <td>${treatment.dosage}</td>
                    <td>
                    <a href= "<c:url value='/edittreatment/${treatment.treatmentId}'/> ">edit</a>
                    <a href= "<c:url value='/deletetreatment/${treatment.treatmentId}'/> ">delete</a>
                    
                    </td>
                </tr
            </c:forEach>
        </table>
    </body>
</html>
