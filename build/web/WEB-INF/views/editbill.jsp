<%-- 
    Document   : editbill
    Created on : Apr 29, 2019, 4:47:46 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>editbill</title>
    </head>
    <body>
        <h1>Edit Bill</h1>
        <form method="POST" >
        <table> 
            <tr><td>Date</td><td> <input type='date' name='date' required="date" value="${bill.date}"></td></tr>
            <tr><td>Debtor Name</td><td> <input type='text' name='debtorsname' value="${bill.debtorsname}"></td></tr>
            <tr><td>Amount Billed</td><td> <input type='number' name='amountbilled' value="${bill.amountbilled}"></td></tr>
            <tr><td>Patient name </td><td> <input type='text' name='patientname' value="${bill.patientname}"></td></tr>
            <tr><td></td><td> <input type='submit' value = "Update"></td></tr>
            
        </table>
    </form>
    </body>
</html>
