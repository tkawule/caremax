<%-- 
    Document   : edituser
    Created on : Apr 29, 2019, 4:53:40 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>user</title>
    </head>
    <body>
        <h1>edituser</h1>
        
        <form method="POST">
            <table>
                <tr><td><input type='text' name='username' placeholder="username" autocomplete="off" value="${user.username}"></td></tr>
                <br>
                <tr><td><input type='password' name='password' placeholder="password" autocomplete="off" value="${user.password}"></td></tr>
                 
                <tr><td><input type='submit' value = "Submit" onclick="return userSaved()" ></td></tr>
                <tr><td><input type='reset' value = "Reset" ></td></tr>
                
             <script>
           function userSaved(){
              if(confirm("Confirm changes to this User")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
