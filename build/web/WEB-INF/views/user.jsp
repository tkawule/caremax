<%-- 
    Document   : user
    Created on : Apr 26, 2019, 1:11:38 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>user</title>
    </head>
    <body>
        <h3>Register a new caremax user</h3>
        
        <form method="POST" action="/CareMax/user">
            <table>
                <tr><td><input type='text' name='username' placeholder="username" autocomplete="off"></td></tr>
                <br>
                <tr><td><input type='password' name='password' placeholder="password" autocomplete="off"></td></tr>
                 
                <tr><td><input type='submit' value = "Submit" onclick="return userSaved()" ></td></tr>
                <tr><td><input type='reset' value = "Reset" ></td></tr>
                
             <script>
           function userSaved(){
              if(confirm("Are you sure you want to save this User")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
