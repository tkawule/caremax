<%-- 
    Document   : patient
    Created on : Jun 24, 2018, 3:26:11 PM
    Author     : Tony Gardner
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="/resources/bootstrap-3.3.7-dist/js/bootstrap.min.js" type="text/javascript"></script>
        
        <title>Add patient</title>
    </head>
    <body>
        <a style="align-content: center" href="<c:url value='/patientlist'/> ">View patients list</a>
        <h2 style="color: blue" align="center">Add new patient</h2>
        <c:choose>
                <c:when test="${empty patient.patientid}">
                 <form method="POST" action="/CareMax/patient" onsubmit="return Validate()" name="vform"> 
                </c:when>
                <c:otherwise>
                 <form method="POST" action="/CareMax/patient" onsubmit="return Validate()" name="vform">
                </c:otherwise>
            </c:choose>
       
            <div>
                
            <table  style="background: whitesmoke" align="center"> 

            <tr><td>Registration Number</td><td> <input type='text' class='form-control' name='registrationnumber'> 
            <div id="reg_error" class="val_error"</div>
            </td></tr>
            <tr><td>Health Care Unit</td><td> <input type='text' name='healthcareunit'>
            <div id="unit_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Patient Name </td><td> <input type='text' name='patientname'>
             <div id="name_error" class="val_error"</div>       
            </td></tr>
            <tr><td>Gender</td><td><select name='gender'>
                        <option value="">--Select Gender--</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </select>
            <div id="gender_error" class="val_error"</div>   
            </td></tr>
            <tr><td>Date of Birth</td><td> <input type='date' name='dateofbirth'>
            <div id="dob_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Home Address </td><td> <input type='text' name='homeaddress'>
            <div id="address_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Occupation </td><td> <input type='text' name='occupation'>
            <div id="occupation_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Religion </td><td> <input type='text' name='religion'>
            <div id="religion_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Tribe </td><td> <input type='text' name='tribe'>
            <div id="tribe_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Next Of Kin </td><td> <input type='text' name='nextofkin'>
            <div id="kin_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Contact of Next Of Kin </td><td> <input type='number' name='contactpersonphone'>
            <div id="contact_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Blood Group</td><td> <input type='text' name='bloodgroup'>
            <div id="blood_error" class="val_error"</div>        
            </td></tr>
            <tr><td>Known Allergies</td><td> <input type='text' name='allergies'>
            <div id="allergy_error" class="val_error"</div>
            </td></tr>
            
            <c:choose>
                <c:when test="${empty patient.patientid}">
                    <input type="hidden" name="patientid" value="0"> 
                </c:when>
                <c:otherwise>
                 <input type="hidden" name="patientid" value="${patient.patientid}">
                </c:otherwise>
            </c:choose>
   
                 <tr><td></td><td> <input type='submit' value = "Add Patient" class="btn btn-success" ></td></tr>
                 <tr><td></td><td> <input type='reset' value = "Reset" ></td></tr>
        </table>
        </form>  
    </body>
   
</html>

