<%-- 
    Document   : edittreatment
    Created on : Apr 29, 2019, 4:50:24 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>edittreatment</title>
    </head>
    <body>
        <h1>edit treatment</h1>
        <form method="post">
        <table>
            <tr><td>Diagnosis</td><td> <input type='text' name='diagnosis' value="${treatment.diagnosis}"></td></tr>
            <tr><td>Treatment</td><td> <textarea type='text' name='treatment' value="${treatment.treatment}"></textarea></td></tr>
            <tr><td>Dosage</td><td> <textarea type='text' name='dosage' value="${treatment.dosage}"></textarea></td></tr>
            <tr><td></td><td> <input type='submit' value = "Update"></td></tr>
            
        </table>
    </form>
    </body>
</html>
