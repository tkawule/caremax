<%-- 
    Document   : home
    Created on : Feb 20, 2019, 2:00:27 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width" initial-scale="1"> 
        <title>HomePage</title>
        
        
        <link href="../resources/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<c:url value="../resources/css/bootstrap.css/>"/>
        
    </head>
    <body data-spy="scoll" data-toggle="#navbarResponsive">
       
            
           <!--- Navigation-->
           <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
               
           </nav>
           <!--- end of navigation-->
          <h1>Hello ${UserName}</h1>
          
        
        
        
    </body>
</html>
