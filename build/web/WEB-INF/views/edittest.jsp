<%-- 
    Document   : edittest
    Created on : Apr 29, 2019, 4:48:35 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>edittest</title>
    </head>
    <body>
        <h1>Edit test</h1>
        <form method="POST">
            <table>
                <tr><td>Test Name</td><td> <input type='text' name='testname' value="${test.testname}"></td></tr>
               
                <tr><td>Test Results</td><td> <input type='text' name='testresults' value="${test.testresults}"></td></tr>
                <tr><td></td><td> <input type='submit' value = "Update Test"></td></tr>
                  
            </table>
        </form>
    </body>
</html>
