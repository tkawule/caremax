<%-- 
    Document   : editdepartment
    Created on : Apr 29, 2019, 4:49:20 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>editdept</title>
    </head>
    <body>
        <h1>Edit department</h1>
        <form method="POST">
            <table>
                <tr><td>Department Id</td><td> <input type='text' name='dep_id' value="${department.dep_id}" readonly="dep_id"></td></tr>
                <tr><td>Department Name</td><td> <input type='text' name='dep_name' value="${department.dep_name}"></td></tr>
                <tr><td>Department Head</td><td> <input type='text' name='dep_head' value="${department.dep_head}"></td></tr>
                <tr><td></td><td> <input type='submit' value = "Update" ></td></tr>
                
                
            </table>
        </form>
    </body>
</html>
