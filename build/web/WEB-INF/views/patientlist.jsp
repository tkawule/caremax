<%-- 
    Document   : patientlist
    Created on : Nov 3, 2018, 2:27:38 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../resources/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <title>patient list</title>
        
<style>
BODY, TABLE, TH, TD, TR, FORM {font: 12px Tahoma,Arial,Helvetica,sans-serif; vertical-align: top}
		FORM {margin: 0px; padding: 0px}
		TEXTAREA, INPUT, SELECT, LABEL, BUTTON {font: 11px Tahoma,Arial,Helvetica,sans-serif }
		TD.label { text-align: right; vertical-align: middle  }
		TABLE {border-collapse: collapse     }
		TH {font: bold; background: #FFF3CB; border: 1px solid #D6D6D6; padding: 5px; text-align: center}
		.bordered {border: 1px solid #D6D6D6 }
		.evenrow  {background-color: #EFEFEF }
</style>

<script>
   function FilterRow(data) {
    console.log("searching for "+data); 
    var filter = data;
    var table = document.getElementById("datatable");
  for (i = 1; i<table.rows.length; i++)
    {
        var rowdata = table.rows[i].innerHTML;
        console.log(rowdata.toUpperCase());
         if(rowdata.toUpperCase().search(data.toUpperCase())>-1) {table.rows[i].style.display="";} 
        else {table.rows[i].style.display="none";} 
    }
    
}
</script> 
        
        
    </head>
    <body>
        
        <a style="align-content: center" href="<c:url value='/patient'/> ">+Add new patient</a>
       
        <hr> 
        <table border="1" width="95%" align="center">
            <tr>
                <td><input style="min-width:95%" type="text" onkeydown="FilterRow(this.value)" placeholder="Search for a Patient"/></td>
            </tr>
        </table><br/>
        <div class="container">
        <table  id="datatable" style="width: 95%" align="center" border="1">
            <thead>
            <tr bgcolor="#cccccc">
            
                <!--<th><b>Patient ID</th>-->
                <th><b>Registration No.</th>
                <th><b>Health care Uni</th>
                <th><b>Patient Name</th>
                <th><b>Gender</td>
                <th><b>Date of Birth</th>
                <th><b>Home Address</th>
                <th><b>Occupation</th>
                <th><b>Religion</th>
                <th><b>Tribe</th>
                <th><b><b>Next Of Kin</th>
                <th><b>Contact of NOK</th>
                <th><b>Blood Group</th>
                <th><b>Known Allergies</th>
                <th><b>Action</th>
            </tr>
</thead>
            <c:forEach items="${patients}" var="patient" varStatus="theCount">
               
            <tr>
                
                <!--<td> ${patient.patientid} </td> -->
                <td> ${patient.registrationnumber} </td>
                <td> ${patient.healthcareunit} </td>
                <td> ${patient.patientname} </td>
                <td> ${patient.gender} </td>
                <td> ${patient.dateofbirth} </td>
                <td> ${patient.homeaddress} </td>
                <td> ${patient.occupation} </td>
                <td> ${patient.religion} </td>
                <td> ${patient.tribe} </td>
                <td> ${patient.nextofkin} </td>
                <td> ${patient.contactpersonphone} </td>
                <td> ${patient.bloodgroup} </td>
                <td> ${patient.allergies} </td>
                <td>
                    <a href= "<c:url value='/editpatient/${patient.patientid}'/> ">edit</a>
                    <a href= "<c:url value='/delete/${patient.patientid}'/> ">delete</a>
                    <a href="<c:url value="/profile/${patient.patientid}"/>">profile</a>
                    
                </td>
                
            </tr> 
                
            </c:forEach>
              
        </table>
        </div>
    </body>
</html>
