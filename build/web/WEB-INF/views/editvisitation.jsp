<%-- 
    Document   : editvisitation
    Created on : May 23, 2019, 11:01:04 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>visits</title>
    </head>
    <body>
        <form method="post">
        <h1>edit visitation</h1>
        <table>
            
            <tr><td>Date</td><td> <input type='date' name='date' autocomplete="off" value="${visitation.date}"></td></tr>
            <tr><td>Present Complaint</td><td> <input type='text' autocomplete="off" name='presentcomplaint' value="${visitation.presentcomplaint}"></td></tr>
            <tr><td>Previous Complaint</td><td> <input type='text' autocomplete="off" name='lastcomplaint' value="${visitation.lastcomplaint}"></td></tr>
            
            <tr><td></td><td> <input type='submit' value = "update"></td></tr>
           
        </table>
        </form>
    </body>
</html>
