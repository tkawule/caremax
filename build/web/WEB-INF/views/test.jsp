<%-- 
    Document   : test
    Created on : Jul 18, 2018, 11:21:00 AM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>test</title>
    </head>
    <body>
        <h1> Add Patient's tests</h1>
        <form method="POST" action="/CareMax/test">
            <table>
                <tr><td>Test Name</td><td> <select type='text' name='testname'>
                    <option value="">--Select Test--</option>
                        <option value="HIV">HIV</option>
                        <option value="BS">BS</option>
                        <option value="Spectum">Spetum</option>
                        <option value="ETC">ETC</option>
                        </select></td></tr>
                <tr><td>Test Results</td><td> <textarea type='text' name='testresults'></textarea></td></tr>
                <tr><td></td><td> <input type='submit' value = "Enter Test" onclick="return testSaved"></td></tr>
                <tr><td></td><td> <input type='reset' value = "reset" ></td></tr>
                    
            </table>
        </form>
             
         <script>
           function testSaved(){
              if(confirm("Are you sure you want to save this test results")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
