/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Treatment {
    
    private int treatmentId;
    private String diagnosis;
    private String treatment;
    private String dosage;
    
    public Treatment() {}
    
    public Treatment(
            int treatmentId
           ,String diagnosis
           ,String treatment
           ,String dosage ){
        
        this.treatmentId = treatmentId;
        this.diagnosis = diagnosis;
        this.treatment = treatment;
        this.dosage = dosage;
    }

    /**
     * @return the treatmentId
     */
    public int getTreatmentId() {
        return treatmentId;
    }

    /**
     * @param treatmentId the treatmentId to set
     */
    public void setTreatmentId(int treatmentId) {
        this.treatmentId = treatmentId;
    }

    /**
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param diagnosis the diagnosis to set
     */
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * @return the treatment
     */
    public String getTreatment() {
        return treatment;
    }

    /**
     * @param treatment the treatment to set
     */
    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    /**
     * @return the dosage
     */
    public String getDosage() {
        return dosage;
    }

    /**
     * @param dosage the dosage to set
     */
    public void setDosage(String dosage) {
        this.dosage = dosage;
    }
    
}
