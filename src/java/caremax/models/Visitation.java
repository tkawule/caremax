
package caremax.models;


public class Visitation {
    private int visitationid;
    private String date;
    private String presentcomplaint;
    private String lastcomplaint;
    
    public Visitation() {}
    
    public Visitation(
             int visitationid
            ,String date
            ,String presentcomplaint
            ,String lastcomplaint ) {
    
        this.visitationid = visitationid;
        this.date = date;
        this.presentcomplaint = presentcomplaint;
        this.lastcomplaint = lastcomplaint;
    
    }

    
    public int getVisitationid() {
        return visitationid;
    }

    
    public void setVisitationid(int visitationid) {
        this.visitationid = visitationid;
    }

    public String getDate() {
        return date;
    }

    
    public void setDate(String date) {
        this.date = date;
    }

    
    public String getPresentcomplaint() {
        return presentcomplaint;
    }

    public void setPresentcomplaint(String presentcomplaint) {
        this.presentcomplaint = presentcomplaint;
    }

    
    public String getLastcomplaint() {
        return lastcomplaint;
    }

    
    public void setLastcomplaint(String previouscomplaint) {
        this.lastcomplaint = previouscomplaint;
    }

    
}
