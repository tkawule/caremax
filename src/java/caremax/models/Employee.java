/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Employee {
    
    private int employeeId;
    private String firstname;
    private String lastname;
    private String reg_date;
    private String dob;
    private String gender;
    private String contact;
    private String profession;
    private String departmentname;
    
    public Employee() {}
    
    public Employee (
            int employeeId
            ,String firstname
            ,String lastname
            ,String reg_date
            ,String dob
            ,String gender
            ,String contact
            ,String profession
            ,String departmentname )
    {
       this.employeeId = employeeId;
       this.firstname = firstname;
       this.lastname = lastname;
       this.reg_date = reg_date;
       this.dob = dob;
       this.gender = gender;
       this.contact = contact;
       this.profession = profession;
       this.departmentname = departmentname;   
    }

    /**
     * @return the employeeId
     */
    public int getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    /**
     * @return the firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @param firstname the firstname to set
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * @return the lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @param lastname the lastname to set
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * @return the reg_date
     */
    public String getReg_date() {
        return reg_date;
    }

    /**
     * @param reg_date the reg_date to set
     */
    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    /**
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the profession
     */
    public String getProfession() {
        return profession;
    }

    /**
     * @param profession the profession to set
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }

    /**
     * @return the departmentname
     */
    public String getDepartmentname() {
        return departmentname;
    }

    /**
     * @param departmentname the departmentname to set
     */
    public void setDepartmentname(String departmentname) {
        this.departmentname = departmentname;
    }

        
}
