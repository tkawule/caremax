/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Appointment {
    
    private int appointmentid;
    private String name;
    private String appointmentdate;
    private String appointmenttime;
    private String message;
    private String contact;
    
    public Appointment(){}
    
    public Appointment(
           int appointmentid
          ,String name
          ,String appointmentdate
          ,String appointmenttime
          ,String message
          ,String contact ) {
    
        
        this.appointmentid = appointmentid;
        this.name = name;
        this.appointmentdate = appointmentdate;
        this.appointmenttime = appointmenttime;
        this.message = message;
        this.contact = contact;
    
    }

    /**
     * @return the appointmentId
     */
    public int getAppointmentid() {
        return appointmentid;
    }

    /**
     * @param appointmentid the appointmentId to set
     */
    public void setAppointmentid(int appointmentid) {
        this.appointmentid = appointmentid;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the date
     */
    public String getAppointmentdate() {
        return appointmentdate;
    }

    
    public void setAppointmentdate(String appointmentdate) {
        this.appointmentdate = appointmentdate;
    }

    /**
     * @return the time
     */
    public String getAppointmenttime() {
        return appointmenttime;
    }

    /**
     * @param time the time to set
     */
    public void setAppointmenttime(String appointmenttime) {
        this.appointmenttime = appointmenttime;
    }

    /**
     * @return the contact
     */
    public String getContact() {
        return contact;
    }

    /**
     * @param contact the contact to set
     */
    public void setContact(String contact) {
        this.contact = contact;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
}
