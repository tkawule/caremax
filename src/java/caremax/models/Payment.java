/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Payment {
    private int paymentId;
    private String date;
    private String amountpaid;
    private String balance;
    private String payeename;
    
    public Payment() {}
    
    public Payment (
    
             int paymentId
            ,String date
            ,String amountpaid
            ,String balance
            ,String payeename ) {
        
        this.paymentId = paymentId;
        this.date = date;
        this.amountpaid= amountpaid;
        this.balance = balance;
        this.payeename = payeename;
    }

    /**
     * @return the paymentId
     */
    public int getPaymentId() {
        return paymentId;
    }

    /**
     * @param paymentId the paymentId to set
     */
    public void setPaymentId(int paymentId) {
        this.paymentId = paymentId;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the amountpaid
     */
    public String getAmountpaid() {
        return amountpaid;
    }

    /**
     * @param amountpaid the amountpaid to set
     */
    public void setAmountpaid(String amountpaid) {
        this.amountpaid = amountpaid;
    }

    /**
     * @return the balance
     */
    public String getBalance() {
        return balance;
    }

    /**
     * @param balance the balance to set
     */
    public void setBalance(String balance) {
        this.balance = balance;
    }

    /**
     * @return the payeename
     */
    public String getPayeename() {
        return payeename;
    }

    /**
     * @param payeename the payeename to set
     */
    public void setPayeename(String payeename) {
        this.payeename = payeename;
    }
    
    
}
