/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Department {
    private int dep_id;
    private String dep_head;
    private String dep_name;
    
    
    public Department() {}
    
    public Department(
             
            int dep_id
           ,String dep_name
           ,String dep_head  ) {
        
        this.dep_id = dep_id;
        this.dep_head = dep_head;
        this.dep_name = dep_name;
    }

    /**
     * @return the dep_id
     */
    public int getDep_id() {
        return dep_id;
    }

    /**
     * @param dep_id the dep_id to set
     */
    public void setDep_id(int dep_id) {
        this.dep_id = dep_id;
    }

    /**
     * @return the dep_head
     */
    public String getDep_head() {
        return dep_head;
    }

    /**
     * @param dep_head the dep_head to set
     */
    public void setDep_head(String dep_head) {
        this.dep_head = dep_head;
    }

    /**
     * @return the dep_name
     */
    public String getDep_name() {
        return dep_name;
    }

    /**
     * @param dep_name the dep_name to set
     */
    public void setDep_name(String dep_name) {
        this.dep_name = dep_name;
    }

    
}
