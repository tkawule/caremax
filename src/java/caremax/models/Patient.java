
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Patient {
    
    private int patientid; 
    private String registrationnumber; 
    private String healthcareunit; 
    private String patientname; 
    private String gender; 
    private String dateofbirth; 
    private String homeaddress; 
    private String occupation; 
    private String religion; 
    private String tribe; 
    private String nextofkin; 
    private String contactpersonphone; 
    private String bloodgroup; 
    private String allergies; 
    
    public Patient(){} 
    
    public Patient(
    int patientid
    ,String registrationnumber  
    ,String healthcareunit  
    ,String patientname  
    ,String gender  
    ,String dateofbirth  
    ,String homeaddress  
    ,String occupation  
    ,String religion  
    ,String tribe   
    ,String nextofkin
    ,String contactpersonphone  
    ,String bloodgroup  
    ,String allergies  ){
    
             this.patientid = patientid; 
             this.registrationnumber = registrationnumber; 
             this.healthcareunit = healthcareunit; 
             this.patientname = patientname; 
             this.gender = gender; 
             this.dateofbirth = dateofbirth; 
             this.homeaddress = homeaddress; 
             this.occupation = occupation; 
             this.religion = religion; 
             this.tribe = tribe; 
             this.nextofkin = nextofkin; 
             this.contactpersonphone = contactpersonphone; 
             this.bloodgroup = bloodgroup; 
             this.allergies = allergies;         
    } 

   
    public int getPatientid() {
        return patientid;
    }


    public void setPatientid(int patientid) {
        this.patientid = patientid;
    }

  
    public String getRegistrationnumber() {
        return registrationnumber;
    }

    /**
     * @param registrationnumber the registrationnumber to set
     */
    public void setRegistrationnumber(String registrationnumber) {
        this.registrationnumber = registrationnumber;
    }

    /**
     * @return the healthcareunit
     */
    public String getHealthcareunit() {
        return healthcareunit;
    }

    /**
     * @param healthcareunit the healthcareunit to set
     */
    public void setHealthcareunit(String healthcareunit) {
        this.healthcareunit = healthcareunit;
    }

    /**
     * @return the patientname
     */
    public String getPatientname() {
        return patientname;
    }

    /**
     * @param patientname the patientname to set
     */
    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the dateofbirth
     */
    public String getDateofbirth() {
        return dateofbirth;
    }

    /**
     * @param dateofbirth the dateofbirth to set
     */
    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    /**
     * @return the homeaddress
     */
    public String getHomeaddress() {
        return homeaddress;
    }

    /**
     * @param homeaddress the homeaddress to set
     */
    public void setHomeaddress(String homeaddress) {
        this.homeaddress = homeaddress;
    }

    /**
     * @return the occupation
     */
    public String getOccupation() {
        return occupation;
    }

    /**
     * @param occupation the occupation to set
     */
    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    /**
     * @return the religion
     */
    public String getReligion() {
        return religion;
    }

    /**
     * @param religion the religion to set
     */
    public void setReligion(String religion) {
        this.religion = religion;
    }

    /**
     * @return the tribe
     */
    public String getTribe() {
        return tribe;
    }

    /**
     * @param tribe the tribe to set
     */
    public void setTribe(String tribe) {
        this.tribe = tribe;
    }

    /**
     * @return the nextofkin
     */
    public String getNextofkin() {
        return nextofkin;
    }

    /**
     * @param nextofkin the nextofkin to set
     */
    public void setNextofkin(String nextofkin) {
        this.nextofkin = nextofkin;
    }

    /**
     * @return the contactpersonphone
     */
    public String getContactpersonphone() {
        return contactpersonphone;
    }

    /**
     * @param contactpersonphone the contactpersonphone to set
     */
    public void setContactpersonphone(String contactpersonphone) {
        this.contactpersonphone = contactpersonphone;
    }

    /**
     * @return the bloodgroup
     */
    public String getBloodgroup() {
        return bloodgroup;
    }

    /**
     * @param bloodgroup the bloodgroup to set
     */
    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    /**
     * @return the allergies
     */
    public String getAllergies() {
        return allergies;
    }

    /**
     * @param allergies the allergies to set
     */
    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }
}
