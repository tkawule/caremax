/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;


/**
 *
 * @author Tony Gardner
 */
public class Test {
     private int testId;
     private String testname;
     private String testresults;
     
     public Test () {}
     
     public Test (
           
             int testId
            ,String testname
            ,String testresults ) {
         
         this.testId = testId;
         this.testname = testname;
         this.testresults = testresults;
     }

    /**
     * @return the testId
     */
    public int getTestId() {
        return testId;
    }

    /**
     * @param testId the testId to set
     */
    public void setTestId(int testId) {
        this.testId = testId;
    }

    /**
     * @return the testname
     */
    public String getTestname() {
        return testname;
    }

    /**
     * @param testname the testname to set
     */
    public void setTestname(String testname) {
        this.testname = testname;
    }

    /**
     * @return the testresults
     */
    public String getTestresults() {
        return testresults;
    }

    /**
     * @param testresults the testresults to set
     */
    public void setTestresults(String testresults) {
        this.testresults = testresults;
    }
      
     
}
