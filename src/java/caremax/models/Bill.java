/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.models;

/**
 *
 * @author Tony Gardner
 */
public class Bill {
    
    private int billId;
    private String date;
    private String debtorsname;
    private String amountbilled;
    private String patientname;
    
    public Bill() {}
    
    public Bill (
           
            int billId
           ,String date
           ,String debtorsname
           ,String amountbilled
           ,String patientname  ) {
        
        this.billId = billId;
        this.date = date;
        this.debtorsname = debtorsname;
        this.patientname = patientname;
    }

    /**
     * @return the billId
     */
    public int getBillId() {
        return billId;
    }

    /**
     * @param billId the billId to set
     */
    public void setBillId(int billId) {
        this.billId = billId;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * @return the debtorsname
     */
    public String getDebtorsname() {
        return debtorsname;
    }

    /**
     * @param debtorsname the debtorsname to set
     */
    public void setDebtorsname(String debtorsname) {
        this.debtorsname = debtorsname;
    }

    /**
     * @return the amountbilled
     */
    public String getAmountbilled() {
        return amountbilled;
    }

    /**
     * @param amountbilled the amountbilled to set
     */
    public void setAmountbilled(String amountbilled) {
        this.amountbilled = amountbilled;
    }

    /**
     * @return the patientname
     */
    public String getPatientname() {
        return patientname;
    }

    /**
     * @param patientname the patientname to set
     */
    public void setPatientname(String patientname) {
        this.patientname = patientname;
    }

   
}
