/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Treatment;
import caremax.services.TreatmentService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc

public class TreatmentController {
 @RequestMapping(value ="/treatmentlist",method = RequestMethod.GET)
     public String getTreatmenttList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Treatment list is being generated"); 
     //Load Data from the service
     List<Treatment> treatmentlist = TreatmentService.getTreatmentlist(); 
     //Mount data on the model and view instance 
     model.addAttribute("treatments", treatmentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "treatmentlist";
   }
    @RequestMapping(value ="/treatment",method = RequestMethod.GET)
     public ModelAndView getAddreatmentoFrm(){
     return new ModelAndView("treatment");
  }
    @RequestMapping(value ="/treatment",method = RequestMethod.POST)
     public ModelAndView updatePatiet(Treatment treatment) throws IOException, SQLException
 {
     System.out.println("Adding treatment "+treatment.getTreatment()+"");
     TreatmentService.addTreatment(treatment);
     return new ModelAndView("treatment","command", new Treatment());
   }
  
    @RequestMapping(value="/edittreatment/{treatmentId}", method = RequestMethod.GET)
      public String editTreatment(@PathVariable("treatmentId") int treatmentId, ModelMap model) throws IOException, SQLException{
      
    Treatment treatment = TreatmentService.getTreatmentById(treatmentId);
     model.addAttribute("treatment", treatment);
     return "edittreatment";
   }
      
        
    @RequestMapping(value ="/edittreatment/{treatmentId}", method = RequestMethod.POST)
    public String editTreatment(Treatment treatment, ModelMap model) throws IOException, SQLException
    {
     TreatmentService.editTreatment(treatment);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Treatment> treatmentlist = TreatmentService.getTreatmentlist(); 
     //Mount data on the model and view instance 
     model.addAttribute("treatments", treatmentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "treatmentlist";
   }
}