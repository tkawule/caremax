/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class HomeController {
    
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String getWelcomePage(ModelMap model){
    
     
    String username = "Client"; 
    model.addAttribute("UserName", username); 
    return "home"; 
    
}
    
}
