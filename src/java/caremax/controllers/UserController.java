/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.User;
import caremax.services.UserService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

/**
 *
 * @author Tony Gardner
 */
@Controller
public class UserController {
    
 @RequestMapping(value ="/user", method = RequestMethod.GET)
 public ModelAndView getUserForm(){
     return new ModelAndView("user");
 }
 
 @RequestMapping(value ="/user", method = RequestMethod.POST)
 public ModelAndView updateUser(User user) throws IOException, SQLException{
     
     System.out.println("Adding user "+user.getUsername()+"");
     UserService.addUser(user); 
     return new ModelAndView("user","command", new User());
   }
 
 @RequestMapping(value ="/userlist",method = RequestMethod.GET)
     public String getUserlist(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("userlist is being generated"); 
     //Load Data from the service
     List<User> userlist = UserService.getUserList(); 
     //Mount data on the model and view instance 
     model.addAttribute("users", userlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "userlist";
   }
     
    @RequestMapping(value="/edituser/{userid}", method = RequestMethod.GET)
      public String editUser(@PathVariable("userid") int userid, ModelMap model) throws IOException, SQLException{
      
     User user = UserService.getUserById(userid);
     model.addAttribute("user", user);
     return "edituser";
    }
      
    @RequestMapping(value ="/edituser/{userid}", method = RequestMethod.POST)
    public String editBill(User user, ModelMap model) throws IOException, SQLException
    {
     UserService.editUser(user);
     //return new ModelAndView("editpatient","command", new Patient());
     List<User> userlist = UserService.getUserList(); 
     //Mount data on the model and view instance 
     model.addAttribute("users", userlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "userlist";
   }
}    