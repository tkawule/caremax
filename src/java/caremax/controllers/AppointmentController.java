/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;


import caremax.models.Appointment;
import caremax.services.AppointmentService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class AppointmentController {
    

@RequestMapping(value ="/appointmentlist",method = RequestMethod.GET)
 public String getAppointmentList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Patientlist is being generated"); 
     //Load Data from the service
     List<Appointment> appointmentlist = AppointmentService.getAppointmentList(); 
     //Mount data on the model and view instance 
     model.addAttribute("appointments", appointmentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "appointmentlist"; 

}
 @RequestMapping(value="/appointment", method = RequestMethod.GET)
 public ModelAndView getAppointmentForm(){
     return new ModelAndView("appointment");
 }
 
 @RequestMapping(value ="/appointment",method = RequestMethod.POST)
      public ModelAndView updateAppointment( Appointment appointment) throws IOException, SQLException{ 
     
     System.out.println("Adding appointment "+appointment.getName()+"");
     AppointmentService.addAppointment(appointment); 
     return new ModelAndView("appointment","command", new Appointment());
     
  }
 @RequestMapping(value="/editappointment/{appointmentid}", method = RequestMethod.GET)
  public String editPatient(@PathVariable("appointmentid") int appointmentid, ModelMap model) throws IOException, SQLException{
      
    Appointment appointment = AppointmentService.getAppointmentById(appointmentid);
    //System.out.println("Editing Patient "+patient.getPatientname()+"");
     
     model.addAttribute("appointment", appointment);
     return "editappointment";
  }
}