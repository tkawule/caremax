/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Payment;
import caremax.services.PaymentService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class PaymentController {
    
  @RequestMapping(value ="/paymentlist",method = RequestMethod.GET)
     public String getPatientList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Paymentlist is being generated"); 
     //Load Data from the service
     List<Payment> paymentlist = PaymentService.getPaymentList(); 
     //Mount data on the model and view instance 
     model.addAttribute("payment", paymentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "paymentlist";  
   }
     @RequestMapping(value="/payment", method = RequestMethod.GET)
     public ModelAndView getPaymentForm(){
        return new ModelAndView("payment"); 
     }
      //method that posts to the db
     @RequestMapping(value="/payment", method = RequestMethod.POST)
     public ModelAndView updatePayment( Payment payment) throws IOException, SQLException{ 
     
     System.out.println("Adding Payment "+payment.getPayeename()+"");
     PaymentService.AddPayment(payment);
     return new ModelAndView("payment","command", new Payment());
}
     
     @RequestMapping(value="/editpayment/{paymentId}", method = RequestMethod.GET)
      public String editPayments(@PathVariable("paymentId") int paymentId, ModelMap model) throws IOException, SQLException{
      
    Payment payment = PaymentService.getPaymentById(paymentId);
     model.addAttribute("payment", payment);
     return "editpayment"; 
  }
      
    @RequestMapping(value ="/editpayment/{paymentId}", method = RequestMethod.POST)
    public String editPayment(Payment payment, ModelMap model) throws IOException, SQLException{
    
     PaymentService.editPayment(payment);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Payment> paymentlist = PaymentService.getPaymentList(); 
     //Mount data on the model and view instance 
     model.addAttribute("payment", paymentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "paymentlist"; 
   }  
}