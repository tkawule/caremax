
package caremax.controllers;

import caremax.models.Test;
import caremax.services.TestService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;



import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class TestController {
  @RequestMapping(value ="/test",method = RequestMethod.GET)
   public ModelAndView getAddTestForm(){
     return new ModelAndView("test");
 }
 
 @RequestMapping(value ="/test",method = RequestMethod.POST)
 public ModelAndView updateTest(@ModelAttribute  Test test, BindingResult result) throws IOException, SQLException{ 
     
     System.out.println("Adding Patient "+test.getTestname()+"");
     TestService.addTest(test); 
     return new ModelAndView("test","command", new Test());
     
   }
 
  @RequestMapping(value ="/testlist",method = RequestMethod.GET)
   public String getTestList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Testlist is being generated"); 
     //Load Data from the service
     List<Test> testlist = TestService.getTestList(); 
     //Mount data on the model and view instance 
     model.addAttribute("tests", testlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "testlist"; 
   }
   @RequestMapping(value="/edittest/{testId}", method = RequestMethod.GET)
      public String editTest(@PathVariable("testId") int testId, ModelMap model) throws IOException, SQLException{
      
    Test test = TestService.getTestById(testId);
     model.addAttribute("test", test);
     return "edittest";
   
   }
    @RequestMapping(value ="/edittest/{testId}", method = RequestMethod.POST)
    public String editTest(Test test, ModelMap model) throws IOException, SQLException
    {
     TestService.editTest(test);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Test> testlist = TestService.getTestList(); 
     //Mount data on the model and view instance 
     model.addAttribute("tests", testlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "testlist";
    }
}