/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Patient;
import caremax.services.PatientService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class PatientController {
    
 @RequestMapping(value ="/patientlist",method = RequestMethod.GET)
 public String getPatientList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Patientlist is being generated"); 
     //Load Data from the service
     List<Patient> patientlist = PatientService.getPatientList(); 
     //Mount data on the model and view instance 
     model.addAttribute("patients", patientlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "patientlist"; 
 }
 
 @RequestMapping(value ="/patient",method = RequestMethod.GET)
 public ModelAndView getAddPatientForm(){
     return new ModelAndView("patient");
    
  }
 
 @RequestMapping(value ="/patient", method = RequestMethod.POST)
 public ModelAndView updatePatient(Patient patient) throws IOException, SQLException
 {
     if(patient.getPatientid()==0) { 
     System.out.println("Adding Patient "+patient.getPatientname()+"");
     PatientService.addPatient(patient); }
     
     else {PatientService.editPatient(patient); } 
     return new ModelAndView("patient","command", new Patient());
  }
 
 @RequestMapping(value="/editpatient/{patientid}", method = RequestMethod.GET)
  public String editPatient(@PathVariable("patientid") int patientid, ModelMap model) throws IOException, SQLException{
      
    Patient patient = PatientService.getPatientById(patientid);
    //System.out.println("Editing Patient "+patient.getPatientname()+"");
     
     model.addAttribute("patient", patient);
     return "editpatient";
  }
  
  @RequestMapping(value ="/editpatient/{patientid}", method = RequestMethod.POST)
  public String editPatient(Patient patient, ModelMap model) throws IOException, SQLException
 {
     PatientService.editPatient(patient);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Patient> patientlist = PatientService.getPatientList(); 
     //Mount data on the model and view instance 
     model.addAttribute("patients", patientlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "patientlist"; 
     
} 
 @RequestMapping(value="/delete/{patientid}", method = RequestMethod.GET)
 public String deletePatient(@PathVariable("patientid") int patientid, ModelMap model) throws IOException, SQLException{
     Patient patient = PatientService.getPatientById(patientid);
     model.clear();
     return "patient";
 }
 @RequestMapping(value ="/profile",method = RequestMethod.GET)
 public ModelAndView getProfile(@PathVariable("patientid") int patientid){
     return new ModelAndView("profile");
  }
}