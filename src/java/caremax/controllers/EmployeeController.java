/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Employee;
import caremax.services.EmployeeService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class EmployeeController {
    
    @RequestMapping(value="/employee", method = RequestMethod.GET)
    public ModelAndView getAddEmployee(){
        return new ModelAndView("employee");
  }
    @RequestMapping(value ="/employeelist",method = RequestMethod.GET)
     public String getPatientList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Patientlist is being generated"); 
     //Load Data from the service
     List<Employee> employeelist = EmployeeService.getEmployeeList(); 
     //Mount data on the model and view instance 
     model.addAttribute("employees", employeelist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "employeelist";
   }
     @RequestMapping(value ="/employee",method = RequestMethod.POST)
      public ModelAndView updateTest( Employee employee) throws IOException, SQLException{ 
     
     System.out.println("Adding Patient "+employee.getFirstname()+"");
     EmployeeService.addEmployees(employee); 
     return new ModelAndView("employee","command", new Employee());
     }
      
    @RequestMapping(value="/editemployee/{employeeId}", method = RequestMethod.GET)
      public String editEmployee(@PathVariable("employeeId") int employeeId, ModelMap model) throws IOException, SQLException{
      
    Employee employee = EmployeeService.getEmployeeById(employeeId);
     model.addAttribute("employee", employee);
     return "editemployee";  
    }
      
    @RequestMapping(value ="/editemployee/{employeeId}", method = RequestMethod.POST)
    public String editEmployee(Employee employee, ModelMap model) throws IOException, SQLException
    {
     EmployeeService.editEmployee(employee);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Employee> employeelist = EmployeeService.getEmployeeList(); 
     //Mount data on the model and view instance 
     model.addAttribute("employees", employeelist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "employeelist"; 
   }
}