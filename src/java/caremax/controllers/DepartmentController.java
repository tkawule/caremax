/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Department;
import caremax.services.DepartmentService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class DepartmentController {
 @RequestMapping(value="/department", method = RequestMethod.GET)
    public ModelAndView getAddDepartment(){
        return new ModelAndView("department");
  }
    
    @RequestMapping(value ="/departmentlist",method = RequestMethod.GET)
     public String getDepartmentList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Patientlist is being generated"); 
     //Load Data from the service
     List<Department> departmentlist = DepartmentService.getDepartmentList(); 
     //Mount data on the model and view instance 
     model.addAttribute("departments", departmentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "departmentlist";
   }
     @RequestMapping(value ="/department",method = RequestMethod.POST)
      public ModelAndView updateTest( Department department) throws IOException, SQLException{ 
     
     System.out.println("Adding Patient "+department.getDep_head()+"");
     DepartmentService.addDepartment(department); 
     return new ModelAndView("department","command", new Department());
     }
      
      @RequestMapping(value="/editdepartment/{dep_id}", method = RequestMethod.GET)
      public String editDeprtment(@PathVariable("dep_id") int dep_id, ModelMap model) throws IOException, SQLException{
      
      Department department = DepartmentService.getDepartmentById(dep_id);
      model.addAttribute("department", department);
      return "editdepartment";
   }
      
   @RequestMapping(value ="/editdepartment/{dep_id}", method = RequestMethod.POST)
    public String editDepartment(Department department, ModelMap model) throws IOException, SQLException {
        
     DepartmentService.editDepartment(department);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Department> departmentlist = DepartmentService.getDepartmentList(); 
     //Mount data on the model and view instance 
     model.addAttribute("departments", departmentlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "departmentlist";
    }
}