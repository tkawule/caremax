/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Visitation;
import caremax.services.VisitationService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class VisitationController {
@RequestMapping(value ="/visitationlist",method = RequestMethod.GET)
     public String getPatientList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Patientlist is being generated"); 
     //Load Data from the service
     List<Visitation> visitationlist = VisitationService.getVisitationList(); 
     //Mount data on the model and view instance 
     model.addAttribute("visitation", visitationlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "visitationlist";
   }
     @RequestMapping(value="/visitation", method = RequestMethod.GET)
      public ModelAndView getAddPatientForm(){
         
         return new ModelAndView("visitation");
     }
     
     @RequestMapping(value="/visitation", method = RequestMethod.POST)
     public ModelAndView updateVisitation(Visitation visitation) throws IOException, SQLException{
         
         VisitationService.addVisitation(visitation); 
   
         return new ModelAndView("visitation","command", new Visitation());   
     }
     
     @RequestMapping(value="/editvisitation/{visitationid}", method = RequestMethod.GET)
     public String editVisitation(@PathVariable("visitationid")int visitationid, ModelMap model) throws IOException, SQLException{
         
         Visitation visitation = VisitationService.getVisitatioById(visitationid);
         model.addAttribute("visitation", visitation);
         return "editvisitation";
     }
     
     @RequestMapping(value ="/editvisitation/{visitationid}", method = RequestMethod.POST)
      public String editPatient(Visitation visitation, ModelMap model) throws IOException, SQLException{
      
     VisitationService.editVisitation(visitation);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Visitation> visitationlist = VisitationService.getVisitationList(); 
     //Mount data on the model and view instance 
     model.addAttribute("visitation", visitationlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "visitationlist"; 
     
} 
   }
