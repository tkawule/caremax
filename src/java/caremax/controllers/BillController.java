/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.controllers;

import caremax.models.Bill;
import caremax.services.BillService;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 *
 * @author Tony Gardner
 */
@Controller
@EnableWebMvc
public class BillController {
    @RequestMapping(value ="/billlist",method = RequestMethod.GET)
     public String getPatientList(ModelMap model) throws IOException, SQLException{
    
     //create a new instance of Model and View
    
     System.out.println("Billlist is being generated"); 
     //Load Data from the service
     List<Bill> billlist = BillService.getBillList(); 
     //Mount data on the model and view instance 
     model.addAttribute("bills", billlist);
     model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "billlist";
   }
   @RequestMapping(value="/bill", method = RequestMethod.GET)
   public ModelAndView getBillForm(){
       return new ModelAndView("bill");
   }
   
   @RequestMapping(value ="/bill",method = RequestMethod.POST)
      public ModelAndView updateBill( Bill bill) throws IOException, SQLException{ 
     
     System.out.println("Adding bill "+bill.getDate()+"");
     BillService.addBill(bill); 
     return new ModelAndView("bill","command", new Bill());
     }
      
    @RequestMapping(value="/editbill/{billId}", method = RequestMethod.GET)
      public String editBill(@PathVariable("billId") int billId, ModelMap model) throws IOException, SQLException{
      
    Bill bill = BillService.getBillById(billId);
     model.addAttribute("bill", bill);
     return "editbill";
    }
      
    @RequestMapping(value ="/editbill/{billId}", method = RequestMethod.POST)
    public String editBill(Bill bill, ModelMap model) throws IOException, SQLException
    {
     BillService.editBill(bill);
     //return new ModelAndView("editpatient","command", new Patient());
     List<Bill> billlist = BillService.getBillList(); 
     //Mount data on the model and view instance 
     model.addAttribute("bills", billlist);
     //model.addAttribute("infopackage", "Packeage data received"); 
     
     //Send back to videw. 
     return "billlist";
    }
}