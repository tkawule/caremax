/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;
import caremax.models.Patient; 
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;


@Service
public class PatientService {
    //adds a patient to a db
    public static Patient addPatient(Patient patient) throws IOException, SQLException{
        
            String sql = "{CALL ADDPATIENT(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            Connection conn = DbConnection.GetMySQLConnection(); 
            CallableStatement stmt; 
            try {
                  stmt = conn.prepareCall(sql); 
                  stmt.setString("vregistrationnumber", patient.getRegistrationnumber());
                  stmt.setString("vhealthcareunit", patient.getHealthcareunit());
                  stmt.setString("vpatientname", patient.getPatientname());
                  stmt.setString("vgender", patient.getGender());
                  stmt.setString("vdateofbirth", patient.getDateofbirth());
                  stmt.setString("vhomeaddress", patient.getHomeaddress());
                  stmt.setString("voccupation", patient.getOccupation());
                  stmt.setString("vreligion", patient.getReligion());
                  stmt.setString("vtribe", patient.getTribe());
                  stmt.setString("vnextofkin", patient.getNextofkin());
                  stmt.setString("vcontactpersonphone", patient.getContactpersonphone());
                  stmt.setString("vbloodgroup", patient.getBloodgroup());
                  stmt.setString("vallergies", patient.getAllergies());
                  
                  stmt.execute(); 
                 
                } catch (SQLException e) { System.out.println("Add Patient Failed: "+e.getMessage()+""); } 
     
    return patient; 
    }
         //GETS PATIENT LIST FROM DB
   public static List<Patient> getPatientList() throws IOException,SQLException{
       String sql = "{CALL GETPATIENTLIST()}";
       Connection conn = DbConnection.GetMySQLConnection(); 
       CallableStatement stmt; 
       ResultSet rs; 
       List<Patient> patients = new ArrayList(); 
       
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){
          Patient patient = new Patient(); 
          patient.setPatientid(rs.getInt("patientid"));
          patient.setRegistrationnumber(rs.getString("registrationnumber"));
          patient.setHealthcareunit(rs.getString("healthcareunit"));
          patient.setPatientname(rs.getString("patientname"));
          patient.setGender(rs.getString("gender"));
          patient.setDateofbirth(rs.getString("dateofbirth"));
          patient.setReligion(rs.getString("religion"));
          patient.setTribe(rs.getString("tribe"));
          patient.setNextofkin(rs.getString("nextofkin"));
          patient.setHomeaddress(rs.getString("homeaddress"));
          patient.setOccupation(rs.getString("occupation"));
          patient.setContactpersonphone(rs.getString("contactpersonphone"));
          patient.setBloodgroup(rs.getString("bloodgroup"));
          patient.setAllergies(rs.getString("allergies"));
          
          System.out.println("Getting"+patient.getPatientid()+" on "+patient.getRegistrationnumber()+"on"+patient.getHealthcareunit()
            +"on"+patient.getPatientname()+"on"+patient.getGender()+"on"+patient.getDateofbirth()+"on"+patient.getHomeaddress()+"on"+patient.getOccupation()
             +"on"+patient.getReligion()+""+patient.getTribe()+"on"+patient.getNextofkin()+"on"+patient.getContactpersonphone()+"on"+patient.getBloodgroup()
             +"on"+patient.getAllergies()); 
          //add other properties of patient here. 
          
          //add current patient object to the list. 
          patients.add(patient); 
        }
  
       }
       catch (SQLException e) { System.out.println("Add Get Patient List Failed: "+e.getMessage()+""); } 
       
     return patients;      
   }
          //EDITS A PATIENT 
   public static Patient editPatient(Patient patient) throws IOException, SQLException{
       String sql = "{CALL EDITPATIENT (?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vpatientid", patient.getPatientid());
           stmt.setString("vregistrationnumber", patient.getRegistrationnumber());
           stmt.setString("vhealthcareunit", patient.getHealthcareunit());
           stmt.setString("vpatientname", patient.getPatientname());
           stmt.setString("vgender", patient.getGender());
           stmt.setString("vdateofbirth", patient.getDateofbirth());         
           stmt.setString("vreligion", patient.getReligion());
           stmt.setString("vtribe", patient.getTribe());
           stmt.setString("vnextofkin", patient.getNextofkin());
           stmt.setString("vhomeaddress", patient.getHomeaddress());
           stmt.setString("voccupation", patient.getOccupation());
           stmt.setString("vcontactpersonphone", patient.getContactpersonphone());
           stmt.setString("vbloodgroup", patient.getBloodgroup());
           stmt.setString("vallergies", patient.getAllergies());
           stmt.executeUpdate();
       }catch (SQLException e) { System.out.println("Edit patient is successful: "+e.getMessage()+""); } 
       
        return patient;   
   }
             //GETS A PATIENT BY ID
   public static Patient getPatientById(int patientid) throws IOException, SQLException{
      String sql = "{CALL GETPATIENTBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
      
       ResultSet rs; 
       Patient patient = new Patient(); 
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("pid", patientid);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          patient.setPatientid(rs.getInt("patientid"));
          patient.setRegistrationnumber(rs.getString("registrationnumber"));
          patient.setHealthcareunit(rs.getString("healthcareunit"));
          patient.setPatientname(rs.getString("patientname"));
          patient.setGender(rs.getString("gender"));
          patient.setDateofbirth(rs.getString("dateofbirth"));
          patient.setReligion(rs.getString("religion"));
          patient.setTribe(rs.getString("tribe"));
          patient.setNextofkin(rs.getString("nextofkin"));
          patient.setHomeaddress(rs.getString("homeaddress"));
          patient.setOccupation(rs.getString("occupation"));
          patient.setContactpersonphone(rs.getString("contactpersonphone"));
          patient.setBloodgroup(rs.getString("bloodgroup"));
          patient.setAllergies(rs.getString("allergies"));
          
          System.out.println("Retrieving for Edit : "+patient.getPatientid()+" on "+patient.getRegistrationnumber()+"on"+patient.getHealthcareunit()
            +"on"+patient.getPatientname()+"on"+patient.getGender()+"on"+patient.getDateofbirth()+"on"+patient.getHomeaddress()+"on"+patient.getOccupation()
             +"on"+patient.getReligion()+""+patient.getTribe()+"on"+patient.getNextofkin()+"on"+patient.getContactpersonphone()+"on"+patient.getBloodgroup()
             +"on"+patient.getAllergies()); 
          //add other properties of patient here. 
          
          //add current patient object to the list. 
           
        }
  
       }
       catch (SQLException e) { System.out.println("Add Get Patient List Failed: "+e.getMessage()+""); } 
       
     return patient;     
      
   }
   
   public static Patient deletePatient(Patient patient) throws IOException, SQLException{
       String sql = "{CALL DELETEPATIENT}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
      
         try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vpatientid", patient.getPatientid());
           stmt.setString("vregistrationnumber", patient.getRegistrationnumber());
           stmt.setString("vhealthcareunit", patient.getHealthcareunit());
           stmt.setString("vpatientname", patient.getPatientname());
           stmt.setString("vgender", patient.getGender());
           stmt.setString("vdateofbirth", patient.getDateofbirth());         
           stmt.setString("vreligion", patient.getReligion());
           stmt.setString("vtribe", patient.getTribe());
           stmt.setString("vnextofkin", patient.getNextofkin());
           stmt.setString("vhomeaddress", patient.getHomeaddress());
           stmt.setString("voccupation", patient.getOccupation());
           stmt.setString("vcontactpersonphone", patient.getContactpersonphone());
           stmt.setString("vbloodgroup", patient.getBloodgroup());
           stmt.setString("vallergies", patient.getAllergies());
           stmt.executeUpdate();
   
         }catch(SQLException e) { System.out.println("Delete Patient Failed: "+e.getMessage()+""); } 
         return patient;
    }
}