/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.Department;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tony Gardner
 */
public class DepartmentService {
    public static Department addDepartment(Department department) throws IOException, SQLException{
        
            String sql = "{CALL ADDDEPARTMENT(?,?)}";
            Connection conn = DbConnection.GetMySQLConnection(); 
            CallableStatement stmt; 
            stmt = conn.prepareCall(sql);
            stmt.setString("vdep_name", department.getDep_name());
            stmt.setString("vdep_head", department.getDep_head());
            stmt.execute();
            
            
            return department;
    }
    
    public static List<Department> getDepartmentList() throws IOException, SQLException{
        
        String sql = "{CALL GETDEPARTMENTLIST()}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        List<Department> departments = new ArrayList(); 
        
        stmt = conn.prepareCall(sql);
        stmt.execute();
        rs = stmt.getResultSet();
        
        while(rs.next()){
            Department department = new Department();
            department.setDep_id(rs.getInt("dep_id"));
            department.setDep_name(rs.getString("dep_name"));
            department.setDep_head(rs.getString("dep_head"));
          
            System.out.println("Getting"+department.getDep_id()+"on"+department.getDep_name()+"on"+department.getDep_head());
            
            departments.add(department);
        }
        return departments;
    } 
    public static Department editDepartment(Department department) throws IOException, SQLException{
        String sql = "{CALL EDITDEPARTMENT(?,?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        
        try{
            stmt = conn.prepareCall(sql);
            stmt.setInt("vdep_id", department.getDep_id());
            stmt.setString("vdep_name", department.getDep_name());
            stmt.setString("vdep_head" , department.getDep_head());
            stmt.executeUpdate();
            
        }catch (SQLException e) { System.out.println("Edit department failed: "+e.getMessage()+""); }
        
        return department;
    } 
    public static Department getDepartmentById(int dep_id) throws IOException, SQLException{
        String sql = "{CALL GETDEPARTMENTBYID(?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        Department department = new Department();
        
        try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("did", dep_id);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          department.setDep_id(rs.getInt("dep_id"));
          department.setDep_name(rs.getString("dep_name"));
          department.setDep_head(rs.getString("dep_head"));  
           } 
       }catch (SQLException e) { System.out.println("Add Get department List Failed: "+e.getMessage()+""); } 
       
     return department;
   }
}       
    
