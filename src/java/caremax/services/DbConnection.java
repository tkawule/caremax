/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Tony Gardner
 */
public class DbConnection {
    
        private static final String DB_DRIVER_CLASS = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/caremax";
	private static final String DB_USERNAME = "root";
	private static final String DB_PASSWORD = "admin";
    
            
    public static Connection GetMySQLConnection() throws IOException, SQLException {
		  
        Connection con = null; 
		try {
			//load the Driver Class
			Class.forName(DB_DRIVER_CLASS);
			//create the connection now
			con = DriverManager.getConnection(DB_URL,DB_USERNAME,DB_PASSWORD);
                        //System.out.println("Database Connection Sucessfull"); 
		    } 
         catch (ClassNotFoundException | SQLException e) {e.getCause(); 
                
                 System.out.println("Database Connection Failed with Message:  "+e.getMessage()+" and cause "+e.getCause()+"");
                }
                if(con !=null){ 
                    
                  System.out.println("Database Connection Successfull.");
                }
		return con;
	}
    
    
    
    
    
    
    
}
