/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.Test;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tony Gardner
 */
public class TestService {
    
    public static Test addTest(Test test) throws IOException, SQLException{
        
        String sql = "{CALL ADDTEST(?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        stmt = conn.prepareCall(sql);
        stmt.setString("vtestname", test.getTestname());
        stmt.setString("vtestresults", test.getTestresults());
        stmt.execute();
        
        return test;
        
    }
    
    public static List<Test> getTestList() throws IOException, SQLException{
        String sql = "{CALL GETTESTLIST}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement  stmt;
        ResultSet rs;
        List<Test> tests = new ArrayList();
        
        try{
            
            stmt = conn.prepareCall(sql);
            stmt.execute();
            rs = stmt.getResultSet();
            
            
            while(rs.next()){    
            
            Test test = new Test(); 
            test.setTestId(rs.getInt("testId"));
            test.setTestname(rs.getString("testname"));
            test.setTestresults(rs.getString("testresults"));
          
            tests.add(test);
          }
        
        }catch(SQLException e) { System.out.println("add get test list failed");}
        
        return tests;
    }
    
    public static Test editTest(Test test) throws IOException, SQLException{
       String sql = "{CALL EDITTEST (?,?,?)}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vtestId", test.getTestId());
           stmt.setString("vtestname", test.getTestname());
           stmt.setString("vtestresults", test.getTestresults());
           
           stmt.executeUpdate();
           
       }catch (SQLException e) { System.out.println("Edit test is successful: "+e.getMessage()+""); } 
       
        return test;
    }
    
    //GETS A BILL BY ID
   public static Test getTestById(int testId) throws IOException, SQLException{
      String sql = "{CALL GETTESTBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
       ResultSet rs; 
       Test test = new Test(); 
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("tid", testId);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          test.setTestId(rs.getInt("testId"));
          test.setTestname(rs.getString("testname"));
          test.setTestresults(rs.getString("testresults"));
          
        } 
    }catch (SQLException e) { System.out.println("Add Get test List Failed: "+e.getMessage()+""); } 
       
     return test;
   }
}
