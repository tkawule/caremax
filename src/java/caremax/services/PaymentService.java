/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.Payment;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tony Gardner
 */
public class PaymentService {
    
    public static Payment AddPayment(Payment payment) throws IOException, SQLException {
           
         String sql ="{ CALL ADDPAYMENT(?,?,?,?)}";
         Connection conn = DbConnection.GetMySQLConnection();
         CallableStatement stmt;
         
         try{
             
             stmt = conn.prepareCall(sql);
         
             stmt.setString("vdate", payment.getDate());
             stmt.setString("vamountpaid", payment.getAmountpaid());
             stmt.setString("vbalance", payment.getBalance());
             stmt.setString("vpayeename", payment.getPayeename());
             stmt.execute();
         
            }catch(SQLException e){ System.out.println("Adding payments failed:" +e.getMessage()+"");}
        
           return payment; 

    }  
    public static List<Payment> getPaymentList() throws IOException, SQLException{
        
        String sql = "{CALL GETPAYMENTLIST()}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        List<Payment> payments = new ArrayList();
        
        
        try{
              
            stmt= conn.prepareCall(sql);
            stmt.execute();
            rs = stmt.getResultSet();
           
            while(rs.next()){
                
                Payment payment = new Payment();
                payment.setPaymentId(rs.getInt("paymentId"));
                payment.setDate(rs.getString("date"));
                payment.setAmountpaid(rs.getString("amountpaid"));
                payment.setBalance(rs.getString("balance"));
                payment.setPayeename(rs.getString("payeename"));
                
                System.out.println("Getting"+payment.getPaymentId()+"on"+payment.getDate()+"on"+payment.getAmountpaid()+"on"+payment.getBalance()+"on"+payment.getPayeename());
                
               payments.add(payment);
            }
                
        }catch(SQLException e){ System.out.println("Add Get Payment List Failed: "+e.getMessage()+""); };
        
        return payments;
    }
    public static Payment editPayment(Payment payment) throws IOException, SQLException{
        String sql = "{CALL EDITPAYMENT(?,?,?,?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        
        try{
            stmt = conn.prepareCall(sql);
            stmt.setInt("vpaymentId", payment.getPaymentId());
            stmt.setString("vdate", payment.getDate());
            stmt.setString("vamountpaid" , payment.getAmountpaid());
            stmt.setString("vbalance" , payment.getBalance());
            stmt.setString("vpayeename" , payment.getPayeename());
            stmt.executeUpdate();
        }catch (SQLException e) { System.out.println("Edit payment is successful: "+e.getMessage()+""); }
        
        return payment;
    } 
    
    public static Payment getPaymentById(int paymentId) throws IOException, SQLException{
      String sql = "{CALL GETPAYMENTBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
       ResultSet rs; 
       Payment payment = new Payment(); 
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("pid", paymentId);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          payment.setPaymentId(rs.getInt("paymentId"));
          payment.setDate(rs.getString("date"));
          payment.setAmountpaid(rs.getString("amountpaid"));
          payment.setBalance(rs.getString("balance"));
          payment.setPayeename(rs.getString("payeename"));
          
        } 
    }catch (SQLException e) { System.out.println("Add Get Bill List Failed: "+e.getMessage()+""); } 
       
     return payment;
   }
}       
        
    

