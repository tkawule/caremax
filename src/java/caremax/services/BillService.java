/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.Bill;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tony Gardner
 */
public class BillService {
    
    public static Bill addBill(Bill bill) throws IOException, SQLException {
        
        try{
           
            String sql ="{CALL ADDBILL(?,?,?,?)}";
            Connection conn = DbConnection.GetMySQLConnection();
            CallableStatement stmt;
            
            stmt = conn.prepareCall(sql);
            
            stmt.setString("vdate", bill.getDate());
            stmt.setString("vdebtorsname", bill.getDebtorsname());
            stmt.setString("vamountbilled", bill.getAmountbilled());
            stmt.setString("vpatientname", bill.getPatientname());
            stmt.execute();
         
        }catch (SQLException e){ System.out.println("Add Bill Failed: "+e.getMessage()+""); } 
        
        return bill;     
    }
    public static List<Bill> getBillList() throws IOException, SQLException{
            String sql ="{CALL GETBILLLIST}";
            Connection conn = DbConnection.GetMySQLConnection();
            CallableStatement stmt;
            ResultSet rs;
            List<Bill> bills = new ArrayList();
            
       try
         {
          stmt = conn.prepareCall(sql);
          stmt.execute();
          rs = stmt.getResultSet();
          
          while(rs.next()){
                Bill bill = new Bill();
                bill.setBillId(rs.getInt("billId"));
                bill.setDate(rs.getString("date"));
                bill.setDebtorsname(rs.getString("debtorsname"));
                bill.setAmountbilled(rs.getString("amountbilled"));
                bill.setPatientname(rs.getString("patientname"));
                
                System.out.println("Getting"+bill.getBillId()+"on"+bill.getDate()+"on"+bill.getDebtorsname()+"on"+bill.getAmountbilled()+"on"+bill.getPatientname());
                
                bills.add(bill);
            }
       } catch(SQLException e) { System.out.println("add get bill list failed");}
       
       return bills;
    }
   
    //EDITS A BILL 
   public static Bill editBill(Bill bill) throws IOException, SQLException{
       String sql = "{CALL EDITBILL (?,?,?,?,?)}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vbillId", bill.getBillId());
           stmt.setString("vdate", bill.getDate());
           stmt.setString("vdebtorsname", bill.getDebtorsname());
           stmt.setString("vamountbilled", bill.getAmountbilled());
           stmt.setString("vpatientname", bill.getPatientname());
           stmt.executeUpdate();
           
       }catch (SQLException e) { System.out.println("Edit bill is successful: "+e.getMessage()+""); } 
       
        return bill;
    }
   
   //GETS A BILL BY ID
   public static Bill getBillById(int billId) throws IOException, SQLException{
      String sql = "{CALL GETBILLBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
       ResultSet rs; 
       Bill bill = new Bill(); 
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("bid", billId);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          bill.setBillId(rs.getInt("billId"));
          bill.setDate(rs.getString("date"));
          bill.setDebtorsname(rs.getString("debtorsname"));
          bill.setPatientname(rs.getString("patientname"));
          bill.setAmountbilled(rs.getString("amountbilled"));
          
        } 
    }catch (SQLException e) { System.out.println("Add Get Bill List Failed: "+e.getMessage()+""); } 
       
     return bill;
   }
}       