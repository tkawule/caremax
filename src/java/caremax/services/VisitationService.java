
package caremax.services;

import caremax.models.Visitation;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class VisitationService {
    public static Visitation addVisitation(Visitation visitation) throws IOException, SQLException{
        
        String sql ="{CALL ADDVISITATION(?,?,?)}";
        CallableStatement stmt;
        Connection conn = DbConnection.GetMySQLConnection();
        stmt = conn.prepareCall(sql);
        
        stmt.setString("vdate", visitation.getDate());
        stmt.setString("vpresentcomplaint", visitation.getPresentcomplaint());
        stmt.setString("vlastcomplaint", visitation.getLastcomplaint());
        
        stmt.execute();
        
        return visitation;
    }
    
    public static List<Visitation> getVisitationList() throws IOException, SQLException{
        String sql = "{CALL GETVISITATIONLIST}";
        Connection conn =DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        List<Visitation> visitations = new ArrayList();
        
        stmt = conn.prepareCall(sql);
        stmt.execute();
        rs = stmt.getResultSet();
        
        while(rs.next()){
            Visitation visitation = new Visitation();
            visitation.setVisitationid(rs.getInt("visitationid"));
            visitation.setDate(rs.getString("date"));
            visitation.setPresentcomplaint(rs.getString("presentcomplaint"));
            visitation.setLastcomplaint(rs.getString("lastcomplaint"));
            
            visitations.add(visitation);
        }
        
        return visitations;
    }
     //EDITS A VISITATION
    public static Visitation editVisitation(Visitation visitation) throws IOException, SQLException{
        String sql = "{CALL EDITVISITATION (?,?,?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        
        try{
          stmt = conn.prepareCall(sql);
          stmt.setInt("vvisitationid", visitation.getVisitationid());
          stmt.setString("vdate", visitation.getDate());
          stmt.setString("vpresentcomplaint", visitation.getPresentcomplaint());
          stmt.setString("vlastcomplaint", visitation.getLastcomplaint());
          stmt.executeUpdate();
        }catch (SQLException e) { System.out.println("Edit visitation is successful: "+e.getMessage()+""); }
        
        return visitation;
    }
    //GETS A VISITATION BY ID
    public static Visitation getVisitatioById(int visitationid) throws IOException, SQLException{
        String sql = "{CALL GETVISITATIONBYID(?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        Visitation visitation = new Visitation();
        
        try{
          stmt = conn.prepareCall(sql);
          stmt.setInt("vid", visitationid);
          stmt.execute();
          rs = stmt.getResultSet();
           
          while(rs.next()){
              visitation.setVisitationid(rs.getInt("visitationid"));
              visitation.setDate(rs.getString("date"));
              visitation.setPresentcomplaint(rs.getString("presentcomplaint"));
              visitation.setLastcomplaint(rs.getString("lastcomplaint"));  
          }
     
        }catch(SQLException e) { System.out.println("Edit visitation is a success "+e.getMessage()+""); }
        return visitation;
    }
}
