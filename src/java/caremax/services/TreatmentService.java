/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.Treatment;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tony Gardner
 */
public class TreatmentService {
    public static Treatment addTreatment (Treatment treatment) throws IOException, SQLException{
        
        String sql ="{CALL ADDTREATMENT(?,?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
      
        stmt = conn.prepareCall(sql);
        stmt.setString("vtreatment", treatment.getTreatment());
        stmt.setString("vdiagnosis", treatment.getDiagnosis());
        stmt.setString("vdosage", treatment.getDosage());
        stmt.execute();
        
        return treatment;
       
    }
   public static List<Treatment> getTreatmentlist() throws IOException, SQLException{
       
       String sql = "{CALL GETTREATMENTLIST}";
       Connection conn = DbConnection.GetMySQLConnection();
       ResultSet rs;
       CallableStatement stmt;
       List<Treatment> treatments = new ArrayList();
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.execute();
           rs = stmt.getResultSet();
           
           while(rs.next()){
               Treatment treatment = new Treatment();
               treatment.setTreatmentId(rs.getInt("treatmentId"));
               treatment.setTreatment(rs.getString("treatment"));
               treatment.setDiagnosis(rs.getString("diagnosis"));
               treatment.setDosage(rs.getString("dosage"));
               
               treatments.add(treatment);
           }
           
       }catch(SQLException e){ System.out.println("Add Get Treatment List Failed: "+e.getMessage()+""); };
       
       return treatments;
   }
   
   //EDITS A TREATMENT 
   public static Treatment editTreatment(Treatment treatment) throws IOException, SQLException{
       String sql = "{CALL EDITTREATMENT (?,?,?,?)}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vtreatmentId", treatment.getTreatmentId());
           stmt.setString("vtreatment", treatment.getTreatment());
           stmt.setString("vdiagnosis", treatment.getDiagnosis());
           stmt.setString("vdosage", treatment.getDosage());
           stmt.executeUpdate();
           
       }catch (SQLException e) { System.out.println("Edit treatment is successful: "+e.getMessage()+""); } 
       
        return treatment;
    }
   
   // GETS A TREATMENT BY ID
    public static Treatment getTreatmentById(int treatmentId) throws IOException, SQLException{
      String sql = "{CALL GETTREATMENTBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
       ResultSet rs; 
       Treatment treatment = new Treatment(); 
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("trid", treatmentId);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          treatment.setTreatmentId(rs.getInt("treatmentId"));
          treatment.setTreatment(rs.getString("treatment"));
          treatment.setDiagnosis(rs.getString("diagnosis"));
          treatment.setDosage(rs.getString("dosage"));
            
        } 
    }catch (SQLException e) { System.out.println("Add Get treatment List Failed: "+e.getMessage()+""); } 
       
     return treatment;
   }
  
}
