/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.Employee;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Tony Gardner
 */
@Service
public class EmployeeService {
    public static Employee addEmployees (Employee employees) throws IOException, SQLException{
        
        String sql = "{ CALL ADDEMPLOYEES (?,?,?,?,?,?,?,?)}";
        Connection conn  = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        
        try{
            stmt = conn.prepareCall(sql);
            stmt.setString("vfirstname", employees.getFirstname());
            stmt.setString("vlastname", employees.getLastname());
            stmt.setString("vreg_date", employees.getReg_date());
            stmt.setString("vdob", employees.getDob());
            stmt.setString("vgender", employees.getGender());
            stmt.setString("vcontact", employees.getContact());
            stmt.setString("vprofession", employees.getProfession());
            stmt.setString("vdepartmentname", employees.getDepartmentname());
            stmt.execute();
            
        }catch (SQLException e) { System.out.println("Add employees Failed: "+e.getMessage()+""); } 

        return employees;
    }
    
    public static List<Employee> getEmployeeList() throws IOException, SQLException{
        String sql = "{CALL GETEMPLOYEESLIST()}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        List<Employee> employees = new ArrayList();
       
        try{
            
            stmt = conn.prepareCall(sql);
            stmt.execute();
            rs = stmt.getResultSet();
            
            while(rs.next()){
                Employee employee = new Employee();
                employee.setEmployeeId(rs.getInt("employeeId"));
                employee.setFirstname(rs.getString("firstname"));
                employee.setLastname(rs.getString("lastname"));
                employee.setReg_date(rs.getString("reg_date"));
                employee.setDob(rs.getString("dob"));
                employee.setGender(rs.getString("gender"));
                employee.setContact(rs.getString("contact"));
                employee.setProfession(rs.getString("profession"));
                employee.setDepartmentname(rs.getString("departmentname"));
                
                employees.add(employee);
                
            }
            
        }  catch (SQLException e) { System.out.println("Add Get Employee List Failed: "+e.getMessage()+""); } 
       
        return employees;
    }
    
          //EDITS AN EMPLOYEE 
    public static Employee editEmployee(Employee employee) throws IOException, SQLException{
       String sql = "{CALL EDITEMPLOYEE (?,?,?,?,?,?,?,?,?)}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vemployeeId", employee.getEmployeeId());
           stmt.setString("vfirstname", employee.getFirstname());
           stmt.setString("vlastname", employee.getLastname());
           stmt.setString("vreg_date", employee.getReg_date());
           stmt.setString("vdob", employee.getDob());
           stmt.setString("vgender", employee.getGender());         
           stmt.setString("vcontact", employee.getContact());
           stmt.setString("vprofession", employee.getProfession());
           stmt.setString("vdepartmentname", employee.getDepartmentname()); 
           stmt.executeUpdate();
       }catch (SQLException e) { System.out.println("Edit employee is successful: "+e.getMessage()+""); } 
       
        return employee;   
    }
   //GETS AN EMPLOYEE BY ID
   
    public static Employee getEmployeeById(int employeeId) throws IOException, SQLException{
      String sql = "{CALL GETEMPLOYEEBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
      
       ResultSet rs; 
       Employee employee = new Employee(); 
       
       try
       {
          stmt = conn.prepareCall(sql);
          stmt.setInt("eid", employeeId);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          employee.setEmployeeId(rs.getInt("employeeId"));
          employee.setFirstname(rs.getString("firstname"));
          employee.setLastname(rs.getString("lastname"));
          employee.setReg_date(rs.getString("reg_date"));
          employee.setDob(rs.getString("dob"));
          employee.setGender(rs.getString("gender"));
          employee.setContact(rs.getString("contact"));
          employee.setProfession(rs.getString("profession"));
          employee.setDepartmentname(rs.getString("departmentname"));     
        }
  
     } catch (SQLException e) { System.out.println("Add Get employee by id Failed: "+e.getMessage()+""); } 
       
     return employee;     
      
   }
   
}