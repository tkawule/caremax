/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;

import caremax.models.User;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Tony Gardner
 */
public class UserService {
    public static User addUser(User user) throws IOException, SQLException{
        String sql = "{CALL ADDUSER(?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        
        try{
           
            stmt = conn.prepareCall(sql);
            stmt.setString("vusername", user.getUsername());
            stmt.setString("vpassword", user.getPassword());
            stmt.execute();
        
        }catch (SQLException e){System.out.println("Add user failed: "+e.getMessage()+"");}
        return user;
    }
    
    public static List<User> getUserList() throws IOException, SQLException{
        String sql = "{CALL GETUSERLIST}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        List<User> users = new ArrayList();
        
        try{
            stmt = conn.prepareCall(sql);
            stmt.execute();
            rs = stmt.getResultSet();
           
            
            while(rs.next()){
                User user = new User();
                user.setUserid(rs.getInt("userid"));
                user.setUsername(rs.getString("username"));
                user.setPassword(rs.getString("password"));
                
                users.add(user);
            }
        }catch(SQLException e) { System.out.println("add get user list failed" + e.getMessage()+"");}
        
        return users;
    } 
    
    public static User editUser(User user) throws IOException, SQLException{
       String sql = "{CALL EDITUSER (?,?,?)}";
       Connection conn = DbConnection.GetMySQLConnection();
       CallableStatement stmt;
       
       try{
           stmt = conn.prepareCall(sql);
           stmt.setInt("vuserid", user.getUserid());
           stmt.setString("vusername", user.getUsername());
           stmt.setString("vpassword", user.getPassword());
           
           stmt.executeUpdate();
           
       }catch (SQLException e) { System.out.println("Edit user failed: "+e.getMessage()+""); } 
       
        return user;
    }
    
    public static User getUserById(int userid) throws IOException, SQLException{
      String sql = "{CALL GETUSERBYID(?)}";
      Connection conn = DbConnection.GetMySQLConnection();
      CallableStatement stmt;
       ResultSet rs; 
       User user = new User(); 
       
       try
       {
          stmt = conn.prepareCall(sql); 
          stmt.setInt("uid", userid);
          stmt.execute();
          rs = stmt.getResultSet(); 
          
          while(rs.next()){ 
          user.setUserid(rs.getInt("userid"));
          user.setUsername(rs.getString("username"));
          user.setPassword(rs.getString("password")); 
          
        } 
    }catch (SQLException e) { System.out.println("Add Get user List Failed: "+e.getMessage()+""); } 
       
     return user;
   }
      
}