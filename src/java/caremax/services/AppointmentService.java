/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caremax.services;
import caremax.models.Appointment;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Tony Gardner
 */
public class AppointmentService {
    
    public static Appointment addAppointment(Appointment appointment) throws IOException, SQLException {
        
        String sql = "{CALL ADDAPPOINTMENT(?,?,?,?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        
        try{
            stmt  = conn.prepareCall(sql);
            stmt.setString("vname", appointment.getName());
            stmt.setString("vappointmentdate", appointment.getAppointmentdate());
            stmt.setString("vappointmenttime", appointment.getAppointmenttime());
            stmt.setString("vmessage", appointment.getMessage());
            stmt.setString("vcontact", appointment.getContact());
            stmt.execute();
        
    }catch (SQLException e){System.out.println("Add appointment failed: "+e.getMessage()+"");}
        
        return appointment;
    }
    
    public static List<Appointment> getAppointmentList() throws IOException,SQLException{
        
        String sql = "{CALL GETAPPOINTMENTLIST}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        List<Appointment> appointments = new ArrayList();
        
        try{
            
            stmt= conn.prepareCall(sql);
            stmt.execute();
            rs = stmt.getResultSet();
            
         while(rs.next()){ 
            Appointment appointment = new Appointment();
            appointment.setAppointmentid(rs.getInt("appointmentid"));
            appointment.setName(rs.getString("name"));
            appointment.setAppointmentdate(rs.getString("appointmentdate"));
            appointment.setAppointmenttime(rs.getString("appointmenttime"));
            appointment.setMessage(rs.getString("message"));
            appointment.setContact(rs.getString("contact"));
            
            appointments.add(appointment);
           }
            
        }catch(SQLException e) { System.out.println("Add Get Appointment List Failed: "+e.getMessage()+""); }
       
        return appointments;
    }
    //EDITS A appointment
    public static Appointment editAppointment(Appointment appointment) throws IOException, SQLException{
        String sql = "{CALL EDITAPPOINTMENT (?,?,?,?,?,?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stml;
        
        try{
          stml = conn.prepareCall(sql);
          stml.setInt("vappointmentid", appointment.getAppointmentid());
          stml.setString("vname", appointment.getName());
          stml.setString("vappointmentdate", appointment.getAppointmentdate());
          stml.setString("vappointmenttime", appointment.getAppointmenttime());
          stml.setString("vmessage", appointment.getMessage());
          stml.setString("vcontact", appointment.getContact());
          stml.executeUpdate();
        }catch (SQLException e) { System.out.println("Edit appointment is successful: "+e.getMessage()+""); }
        
        return appointment;
  }
    
    //GETS AN APPOINTMENT BY ID
    public static Appointment getAppointmentById(int appointmentid) throws IOException, SQLException{
        String sql = "{CALL GETAPPOINTMENTBYID(?)}";
        Connection conn = DbConnection.GetMySQLConnection();
        CallableStatement stmt;
        ResultSet rs;
        Appointment appointment = new Appointment();
        
        try{
          stmt = conn.prepareCall(sql);
          stmt.execute();
          rs = stmt.getResultSet();
          stmt.setInt("aid", appointmentid);
          
          
          while(rs.next()){ 
          appointment.setAppointmentid(rs.getInt("appointmentid"));
          appointment.setName(rs.getString("name"));
          appointment.setAppointmentdate(rs.getString("appointmentdate"));
          appointment.setAppointmenttime(rs.getString("appointmenttime"));
          appointment.setMessage(rs.getString("message"));
          appointment.setContact(rs.getString("contact"));
          } 
         
        }catch(SQLException e) { System.out.println("Edit appointment failed "+e.getMessage()+""); }
        return appointment;
    }
}