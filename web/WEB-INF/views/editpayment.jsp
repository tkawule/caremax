<%-- 
    Document   : editemployee
    Created on : Apr 29, 2019, 3:49:41 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>editpayment</title>
    </head>
    <body>
        <h1>edit payment</h1>
        <div class="topnav" style="color: blue">
            
        </div>
        
        <form  method="POST">
        <table>
            <tr><td>Payment Id</td><td><input type="text" name="paymentId" value='${payment.paymentId}' readonly="paymentId"></td></tr>
            <tr><td>Date</td><td><input type="date" name="date" value='${payment.date}'></td></tr>
            <tr><td>Amount Paid</td><td><input type="number" name="amountpaid" value="${payment.amountpaid}"></td></tr>
            <tr><td>Balance<td><input type="number" name="balance" value="${payment.balance}"></td></tr>
            <tr><td>Payee name<td><input type="text" name="payeename" value="${payment.payeename}"></td></tr>
            
            <tr><td></td><td> <input type='submit' onclick="return paymentEdit();" value = "update"></td></tr>
            <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr>
        </table>
        </form>
        
        <script>
           function paymentEdit(){
              if(confirm("Are you sure you want to save the changes to this payment")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
