<%-- 
    Document   : bill
    Created on : Jul 18, 2018, 1:51:02 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bill Page</title>
    </head>
    <body>
        <h1>Attach Bill To A Patient</h1>
    <form method="POST" action="/CareMax/bill">
        <table> 
            <tr><td>Date</td><td> <input type='date' name='date' required="date"></td></tr>
            <tr><td>Debtor Name</td><td> <input type='text' name='debtorsname'></td></tr>
            <tr><td>Amount Billed</td><td> <input type='number' name='amountbilled'></td></tr>
            <tr><td>Patient name </td><td> <input type='text' name='patientname'></td></tr>
            <tr><td></td><td> <input type='submit' value = "Enter Bill"></td></tr>
            <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr>
        </table>
    </form>
    </body>
    <script>
    function billSaved(){
              if(confirm("Are you sure you want to submit this Bill")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
    </script>
</html>
