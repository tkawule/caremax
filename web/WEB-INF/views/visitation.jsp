<%-- 
    Document   : visit
    Created on : Nov 21, 2018, 1:56:50 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title> visits</title>
    </head>
    <form method="POST" action="/CareMax/visitation">
        <table>
            <a style="align-content: center" href="<c:url value='/visitationlist'/> ">view visitations</a>
            <h2>Add patients visit page</h2>
            
            <div> 
            <!--<tr><td>Visitation ID </td><td> <input type='text' value=${visitation.visitationid} name='visitationid'></td></tr>-->
                
                <tr><td>Date</td><td> <input type='date' name='date' autocomplete="off"></td></tr>
                <tr><td>Present Complaint</td><td> <input type='text' autocomplete="off" name='presentcomplaint'></td></tr>
                <tr><td>Previous Complaint</td><td> <input type='text' autocomplete="off" name='lastcomplaint'></td></tr>
            
                <tr><td></td><td> <input type='submit' value = "Enter visit"></td></tr>
                <tr><td> </td><td> <input type='reset' value = "Reset" ></td></tr>
                
        </table>
    </form>
    <body>
        
         <script>
           function visitationSaved(){
              if(confirm("Are you sure you want to save this visitation")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
