<%-- 
    Document   : editappointment
    Created on : Jun 5, 2019, 12:33:18 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>editappoitment</title>
    </head>
    <body>
        <h1>edit appointment</h1>
        
        <form method="POST">
            
            <table> 
                <tr><td> Name</td><td> <input type='text' name='name' id="name" value="${appointment.name}"></td></tr>
                <p class="error"></p>
                <tr><td>Date of an appointment</td><td> <input type='date' name='appointmentdate' id="appointmentdate" value="${appointment.appointmentdate}"></td></tr>
                <p class="error"></p>
                <tr><td>Time of an appointment</td><td> <input type='time' name='appointmenttime' id="appointmenttime" value="${appointment.appointmenttime}"></td></tr>
                <p class="error"></p>
                <tr><td>Message</td><td> <textarea type='text' name='message' id="message" value="${appointment.message}" ></textarea></td></tr>
                <p class="error"></p>
                <tr><td>Contact</td><td> <input type='number' name='contact' id="contact" value="${appointment.contact}"></td></tr>
                <p class="error"></p>
                <tr><td></td><td> <input type='submit' value = "Update"></td></tr>
                      
            </table>
          </form>
    </body>
</html>
