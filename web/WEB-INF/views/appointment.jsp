<%-- 
    Document   : add appointment
    Created on : Aug 6, 2018, 12:17:26 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Appointment</title>
    
    </head>
    <body>
        <h3>Please place an appointment</h3>
        <form method="POST" action="/CareMax/appointment">
            
            <table> 
                <tr><td> Name</td><td> <input type='text' name='name' id="name" ></td></tr>
                <p class="error"></p>
                <tr><td>Date of an appointment</td><td> <input type='date' name='appointmentdate' id="appointmentdate"></td></tr>
                <p class="error"></p>
                <tr><td>Time of an appointment</td><td> <input type='time' name='appointmenttime' id="appointmenttime"></td></tr>
                <p class="error"></p>
                <tr><td>Message</td><td> <textarea type='text' name='message' id="message" ></textarea></td></tr>
                <p class="error"></p>
                <tr><td>Contact</td><td> <input type='number' name='contact' id="contact"></td></tr>
                <p class="error"></p>
                <tr><td></td><td> <input type='submit' value = "Submit" onclick="return appointmentSaved();"></td></tr>
                <tr><td></td><td> <input type='reset' value = "Reset" ></td></tr>       
   </table>
  </form>
    <script>
                    function appointmentSaved(){
              if(confirm("Are you sure you want to submit this appointment")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
           
           function Validation(){
           var name = document.getElementById("name");
           var appointmentdate = document.getElementById("appointmentdate");
           var appointmenttime = document.getElementById("appointmenttime");
           var message = document.getElementById("message ");
           var contact = document.getElementById("contact");
           console.log(name.value);
           return false;
           
          }
   </script>
  </body>
</html>
