<%-- 
    Document   : employeelist
    Created on : Dec 27, 2018, 12:04:34 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../resources/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        
        
<style>
BODY, TABLE, TH, TD, TR, FORM {font: 12px Tahoma,Arial,Helvetica,sans-serif; vertical-align: top}
		FORM {margin: 0px; padding: 0px}
		TEXTAREA, INPUT, SELECT, LABEL, BUTTON {font: 11px Tahoma,Arial,Helvetica,sans-serif }
		TD.label { text-align: right; vertical-align: middle  }
		TABLE {border-collapse: collapse     }
		TH {font: bold; background: #FFF3CB; border: 1px solid #D6D6D6; padding: 5px; text-align: center}
		.bordered {border: 1px solid #D6D6D6 }
		.evenrow  {background-color: #EFEFEF }
</style>

<script>
   function FilterRow(data) {
    console.log("searching for "+data); 
    var filter = data;
    var table = document.getElementById("datatable");
  for (i = 1; i<table.rows.length; i++)
    {
        var rowdata = table.rows[i].innerHTML;
        console.log(rowdata.toUpperCase());
         if(rowdata.toUpperCase().search(data.toUpperCase())>-1) {table.rows[i].style.display="";} 
        else {table.rows[i].style.display="none";} 
    }
    
}
</script> 
            
    </head>
    <body>
        <div class="container">
        <a style="align-content: center" href="<c:url value='/employee'/> ">+Add new employee</a>
       
        <hr> 
        <table border="1" width="95%" align="center">
            <tr>
                <td><input style="min-width:95%" type="text" onkeydown="FilterRow(this.value)" placeholder="Search for an employee"/></td>
            </tr>
        </table><br/>
        
        <table  id="datatable" style="width: 95%" align="center" border="1">
            <thead>
            <tr bgcolor="#cccccc">
           
            <!--<th><b>Employee Id</th>-->
            <th><b>First Name</th>
            <th><b>Last Name</th>
            <th><b>Registration Date</th>
            <th><b>Date of Birth</th>
            <th><b>Gender</th>
            <th><b>Contact</th>
            <th><b>Profession</th>
            <th><b>Department Name</th>
             <th><b>Action</th>
           </tr>
            
            
            <c:forEach items="${employees}" var="employee" varStatus="theCount">
            <tr>
                <!--<td>${employee.employeeId}</td>-->
                <td>${employee.firstname}</td>
                <td>${employee.lastname}</td>
                <td>${employee.reg_date}</td>
                <td>${employee.dob}</td>
                <td>${employee.gender}</td>
                <td>${employee.contact}</td>
                <td>${employee.profession}</td>
                <td>${employee.departmentname}</td>
                <td>
                    <a href= "<c:url value='/editemployee/${employee.employeeId}'/> ">edit</a>
                    <a href= "<c:url value='/deleteemployee/${employee.employeeId}'/> ">delete</a>  
                </td>
            </tr>
            
            </c:forEach>
        </thead>
        </table>
        </div>
    </body>
</html>
