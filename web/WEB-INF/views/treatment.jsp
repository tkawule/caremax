<%-- 
    Document   : treatment
    Created on : Jul 18, 2018, 1:30:58 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Treatment</title>
    </head>
    
    <body>
        <h1>Add treatment form</h1>
    <form method="post" action="/CareMax/treatment">
        <table>
            
            <tr><td>Diagnosis</td><td> <input type='text' name='diagnosis'></td></tr>
            <tr><td>Treatment</td><td> <textarea type='text' name='treatment'></textarea></td></tr>
            <tr><td>Dosage</td><td> <textarea type='text' name='dosage'></textarea></td></tr>
            <tr><td></td><td> <input type='submit' value = "Submit"></td></tr>
            <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr>
            
        </table>
    </form>
         <script>
           function treatmentSaved(){
              if(confirm("Are you sure you want to save this treatment")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
