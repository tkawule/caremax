<%-- 
    Document   : payment
    Created on : Jul 18, 2018, 4:32:24 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>payment Page</title>
    </head>
    <body>
        <h1>Payments</h1>
        <form method="POST" action="/CareMax/payment">
        <table>
            <tr><td>Date</td><td> <input type='date' name='date'></td></tr>
            <tr><td>Amount Paid</td><td> <input type='number' name='amountpaid'></td></tr>
            <tr><td>Balance</td><td> <input type='number' name='balance'></td></tr>
            <tr><td>Payee Name</td><td> <input type='text' name='payeename'></td></tr>
            <tr><td></td><td> <input type='submit' value = "Submit" onclick="return PaymentSaved()" ></td></tr>
            <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr>
        </table>
    </form>
         <script>
           function PaymentSaved(){
              if(confirm("Are you sure you want to save this payment")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
