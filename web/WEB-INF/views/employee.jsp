<%-- 
    Document   : employees
    Created on : Nov 29, 2018, 2:15:46 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>employees page</title>
    </head>
    
    <body>
        <div class="topnav" style="color: blue">
            
        </div>
        <a href="<c:url value='/employeelist'/>">view employees</a>
        
        <h1> add employee page</h1>
        <form  method="POST" action="/CareMax/employee">
        <table>
            <tr><td>First Name</td><td><input type="text" name="firstname" </td></tr>
            <tr><td>Last Name</td><td><input type="text" name="lastname"</td></tr>
            <tr><td>Registration date<td><input type="date" name="reg_date"</td></tr>
            <tr><td>Date of Birth<td><input type="date" name="dob"</td></tr> 
            <tr><td>Gender<td><select type="text" name="gender">
                   <option value="">--Select Gender--</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </select></td></tr>
            <tr><td>Contact<td><input type="text" name="contact"</td></tr>
            <tr><td>Profession<td><input type="text" name="profession"</td></tr>
            <tr><td>Department Name<td><input type="text" name="departmentname"</td></tr>
            <tr><td></td><td> <input type='submit' onclick="return employeeSaved()" value = "Submit"></td></tr>
            <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr> 
        </table>
        </form>
        
        <script>
           function employeeSaved(){
              if(confirm("Are you sure you want to save this employee")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
