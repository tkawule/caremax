<%-- 
    Document   : editemployee
    Created on : Apr 29, 2019, 3:49:41 PM
    Author     : Tony Gardner
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>editemployee</title>
    </head>
    <body>
        <h1>edit employee information</h1>
        <div class="topnav" style="color: blue">
            
        </div>
        
        <form  method="POST">
        <table>
            <tr><td>First Name</td><td><input type="text" name="firstname" value='${employee.firstname}'></td></tr>
            <tr><td>Last Name</td><td><input type="text" name="lastname" value="${employee.lastname}"></td></tr>
            <tr><td>Registration date<td><input type="date" name="reg_date" value="${employee.reg_date}"></td></tr>
            <tr><td>Date of Birth<td><input type="date" name="dob" value="${employee.dob}"></td></tr> 
            <tr><td>Gender<td><select type="text" name="gender" value='${employee.gender}'>
                   <option value="">--Select Gender--</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        </select></td></tr>
            <tr><td>Contact<td><input type="text" name="contact" value="${employee.contact}"></td></tr>
            <tr><td>Profession<td><input type="text" name="profession" value="${employee.profession}"></td></tr>
            <tr><td>Department Name<td><input type="text" name="departmentname" value="${employee.departmentname}"></td></tr>
            <tr><td></td><td> <input type='submit' onclick="return employeeEdit();" value = "update"></td></tr>
            <tr><td></td><td> <input type='button' value = "Cancel" ></td></tr> 
        </table>
        </form>
        
        <script>
           function employeeEdit(){
              if(confirm("Are you sure you want to save the changes to this employee")){
                  alert("You selected ok");
                  return true;
              }
               else{
                   alert("You selacted cancel");
                   return false;
               }
           }
            </script>
    </body>
</html>
